"""Setup project."""
from setuptools import setup

setup(
    packages=['app'],
    name='imtoolazy',
    version='1.0',
    install_requires=[
        'youtube_dl',
        'youtube-search-python',
        'click',
        'crayons'
    ],
    entry_points={
        'console_scripts': [
            'imtoolazy=app:run'
        ]
    }
)
