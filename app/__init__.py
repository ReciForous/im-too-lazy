"""
Im Too Lazy.

A simple console application to search and download videos
from youtube.

@author: Mohamed Zain Riyaz
@maintainer: Mohamed Zain Riyaz
@email: mohamedzainriyaz@gmail.com
@license: WTFPL Non-commercial Private License
"""
import os
import configparser
import click
from youtubesearchpython import SearchVideos
import youtube_dl
import crayons


class YtdlLogger(object):
    """Youtube Dl logger class."""

    def debug(self, msg):
        """Debug."""
        pass

    def warning(self, msg):
        """Warning."""
        pass

    def error(self, msg):
        """Error."""
        print(msg)


def printProgressBar(percent, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    filledLength = int(length * percent // 100)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'{prefix} |{bar}| {percent}% {suffix}\r', end=''),
    # Print New Line on Complete
    if percent == 100:
        print()


def ytdl_hook(d):
    """Youtube-dl hook to show progress bar."""
    if d['status'] == 'finished':
        file_tuple = os.path.split(os.path.abspath(d['filename']))
        print(f"Done downloading {file_tuple[1]}, starting conversion...")
    if d['status'] == 'downloading':
        printProgressBar(float(d['_percent_str'].split('%')[0]), 100, suffix=d['_eta_str'])


def read_from_config(profile_name, field):
    """Read from a .cfg file to get basic configurations."""
    config = configparser.ConfigParser()
    config.read(os.path.expanduser('~/.config/imtoolazy/.imtoolazy.cfg'))
    if profile_name not in config.sections():
        raise click.ClickException(f'Profile {profile_name} not in .cfg file')
    if field not in config[profile_name]:
        raise click.ClickException(f'Field {field} not in .cfg file')
    return config[profile_name][field]


def show_search_results(results):
    """Stub function to print search results."""
    for result in results:
        title = result['title']
        link = result['link']
        index = results.index(result)

        click.echo(
            f'{index}: {crayons.green("Title:")} {title}\n' +
            f'{crayons.red("   Link:")} {link}\n'
        )


@click.command()
@click.option('--xtract-audio/--video-only', default=False)
@click.option('-f', '--save-format', default=None)
@click.option('-d', '--path', type=click.Path(exists=True))
@click.option('--show-options/--no-options', default=False)
@click.option('--search-only/--no-search', default=False)
@click.option('-c', '--cookiefile', type=click.Path(exists=True))
@click.option('--use-cookies/--no-cookies', default=False)
@click.option('--use-credentials/--no-credentials', default=False)
@click.option('-p', '--profile', default='default')
@click.option('-m', '--max-results', default=10)
@click.argument('videoname')
def run(
    xtract_audio,
    save_format,
    path,
    show_options,
    search_only,
    cookiefile,
    use_cookies,
    use_credentials,
    profile,
    max_results,
    videoname
):
    """Simple console application to search & download videos from youtube."""
    click.echo("Searching...")
    search = SearchVideos(
        videoname,
        offset=1,
        mode='dict',
        max_results=max_results
    )

    results = search.result()['search_result']

    audio_format_default = 'mp3'
    video_format_default = 'mp4'

    video_index = 0

    if search_only:
        show_search_results(results)

    else:
        # Basic youtube-dl configs
        ytdl_opt = {
            'logger': YtdlLogger(),
            'progress_hooks': [ytdl_hook],
            'prefer_ffmpeg': True,
            'age_limit': 18
        }

        # Credential configs
        if use_credentials:
            try:
                ytdl_opt['username'] = read_from_config(profile, 'username')
                ytdl_opt['password'] = read_from_config(profile, 'password')
            except click.ClickException as e:
                click.echo(crayons.red(e) + '\nAborting!')
                return 1

        # Cookie configs
        if not use_cookies:
            pass
        elif use_cookies and cookiefile:
            ytdl_opt['cookiefile'] = cookiefile
        else:
            try:
                ytdl_opt['cookiefile'] = read_from_config(
                    profile,
                    'cookiefile'
                )
            except click.ClickException as e:
                click.echo(crayons.red(e) + '\nAborting!')
                return 1

        # Xtract audio and postprocessor configs
        if xtract_audio:
            # Set postprocessor configurations here to extract audio
            if not save_format:
                save_format = audio_format_default
            postprocessors = [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': save_format,
                'preferredquality': '192',
            }]

            ytdl_opt['format'] = 'bestaudio/best'
        else:
            if not save_format:
                save_format = video_format_default
            postprocessors = [{
                'key': 'FFmpegVideoConvertor',
                'preferedformat': save_format
            }]
            # Limit to 480px
            ytdl_format = 'bestvideo[height<=480]+bestaudio/best[height<=480]'
            ytdl_opt['format'] = ytdl_format

        ytdl_opt['postprocessors'] = postprocessors

        # path configs
        if path:
            ytdl_opt['outtmpl'] = f'{path}%(title)s.%(ext)s'
        else:
            try:
                ytdl_opt['outtmpl'] = (
                    read_from_config(profile, 'path') +
                    '%(title)s.%(ext)s'
                )
            except click.ClickException as e:
                click.echo(crayons.red(e) + '\nAborting!')
                return 1

        # option configs
        if show_options:
            show_search_results(results)
            choices = [x for x in range(len(results))]

            video_index = click.prompt(
                'Please Enter a option: ',
                type=int
            )

            while video_index not in choices:
                video_index = click.prompt(
                    'Invalid Option! Please Enter a option: ',
                    type=int
                )

        result = results[video_index]

        click.echo(f'Selected video: {result["title"]}')
        click.echo('Starting download...')

        with youtube_dl.YoutubeDL(ytdl_opt) as ytdl:
            ytdl.download([result['link']])


if __name__ == '__main__':
    run()
