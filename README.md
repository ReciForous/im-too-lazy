# IM TOO LAZY

Executable python application to download random youtube videos using yotube-dl & youtube-search-python packages.

Beacause i'm too lazy to actually search up youtube videos & copy paste the video link for youtube-dl at this point.

### Dependencies

- Python3 
- Pip3
- click
- youtube-dl
- youtube-search-python

### How do I get set up?

- Run setup.py with the following command

```
// working dir : ./

pip install -e ./
```

- Create a .imtoolazy.cfg file

```
mkdir -p ~/.config/imtoolazy/

touch ~/.config/imtoolazy/.imtoolazy.cfg

echo "[default]
username=$USERNAMEHERE
password=$PASSWORDHERE
path=path/to/download/to
cookiefile=path/to/cookie/file
" > ~/.config/imtoolazy/.imtoolazy.cfg
```

### How to use

```
imtoolazy 'Search video name here'
```

### Optional arguments

- `-h`, `--help`: Display cli help.

- `-f`, `--save_format`: Provide format to save, defaults to mp3.

- `-d`, `--path`: path to custom download folder.

- `-c`, `--cookiefile`: path to custom cookiefile.

- `-p`, `--profile`: set profile to use for the .cfg file. 

- `-f`, `--save-format`: set format to save video or audio file, video-default - mp4, audio-default - mp3. 

- `-m`, `--max-results`: set the number of results retrieved, defaults to 10.

### Flags

- `--xtract-audio`: Default = False, Extract audio only.

- `--show-options`: Default = False, Shows results & promps user to select options else default selection is first result.

- `--use-credentials`: Default = False, Uses credentials in the cfg file.

- `--search-only`: Default = False, search & show results, then abort.

### Who do I talk to? ###

- [Mohamed Zain Riyaz](mailto:mohamedzainriyaz@gmail.com)

### License

Non-Commercial Use WTFPL
